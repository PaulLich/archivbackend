from rest_framework import status
from rest_framework.exceptions import APIException


class ArchivDataExceptions(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'internal server error - 2'

    def __init__(self, detail=None, error_code=None):
        self.detail = (detail or self.default_detail)
        self.error_code = error_code


class ValidationError(APIException):
    status_code = 422
    default_detail = 'validation error: %s'

    def __init__(self, data, detail=None, error_code=None):
        self.detail = (detail or self.default_detail) % data
        self.error_code = error_code


class DBError(APIException):
    status_code = status.HTTP_503_SERVICE_UNAVAILABLE
    default_detail = 'database error'

    def __init__(self, detail=None, error_code=None):
        self.detail = (detail or self.default_detail)
        self.error_code = error_code


class InvalidQueryParams(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'one or more query parameters have invalid values'

    def __init__(self, detail=None, error_code=None):
        self.detail = detail or self.default_detail
        self.error_code = error_code


class KeyExists(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = 'KeyExists error: %s'

    def __init__(self, data, detail=None, error_code=None):
        self.detail = (detail or self.default_detail) % data
        self.error_code = error_code
    pass


class NotInDB(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'key %s '

    def __init__(self, key=None, detail=None, error_code=None):
        if key is None and detail is None:
            self.detail = self.default_detail % ''

        else:
            self.detail = (detail or self.default_detail) % key
        self.error_code = error_code


class PermissionDenied(object):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = ''

    def __init__(self, key=None, detail=None, error_code=None):
        if key is None and detail is None:
            self.detail = self.default_detail % ''

        else:
            self.detail = (detail or self.default_detail) % key
        self.error_code = error_code
