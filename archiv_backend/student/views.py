from django.forms import model_to_dict
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated

from archiv_backend.authentication import log_request_information, TokenAuth
from archiv_backend.response import content_response, string_response
from archiv_backend.student.models import Student, StudentOrder
from dao import StudentDAO, StudentOrderDAO


class StudentSet(viewsets.ViewSet):

    authentication_classes = (TokenAuth,)
    permission_classes = (IsAuthenticated,)

    def __init__(self, **kwargs):
        super(StudentSet, self).__init__(**kwargs)
        self.student_dao = StudentDAO()

    def get(self, request):
        log_request_information(request, "basic")
        params = self.student_dao.get_search_params(request)

        if not params:
            students = self.student_dao.get_all_by_request(Student)
        else:
            students = self.student_dao.get_by_search_fields(Student, params)

        return content_response(students, status_code=status.HTTP_200_OK)

    def post(self, request):
        log_request_information(request, "basic")

        payload = request.data

        student = self.student_dao.create_or_update_student(payload)

        return content_response(model_to_dict(student), status_code=status.HTTP_201_CREATED)

    def put(self, request, id):
        log_request_information(request, "basic")

        payload = request.data

        student = self.student_dao.create_or_update_student(payload, id)

        return content_response(model_to_dict(student), status_code=status.HTTP_201_CREATED)

    @staticmethod
    def retrieve(request, student_id):
        log_request_information(request, "basic")
        student = get_object_or_404(Student, pk=int(student_id))

        return content_response(model_to_dict(student), status_code=status.HTTP_200_OK)

    def search(self, request):
        log_request_information(request, "basic")
        students = self.student_dao.perform_fts(request)

        return content_response(students, status_code=status.HTTP_200_OK)

    def delete(self, request, student_id):
        log_request_information(request, "basic")
        self.student_dao.delete_entry(Student, student_id)

        #logger.info("Successfully deleted student %s" % student_id)
        return string_response("Successfully deleted student %s" % student_id, status_code=status.HTTP_200_OK)


class StudentOrderSet(viewsets.ViewSet):

    authentication_classes = (TokenAuth,)
    permission_classes = (IsAuthenticated,)

    def __init__(self, **kwargs):
        super(StudentOrderSet, self).__init__(**kwargs)
        self.student_order_dao = StudentOrderDAO()

    def get(self, request):
        log_request_information(request, "basic")
        params = self.student_order_dao.get_search_params(request)

        return_list = []

        if not params:
            student = self.student_order_dao.get_all_by_request(StudentOrder)
        else:
            student = self.student_order_dao.get_by_search_fields(StudentOrder, params)

        for order in student:
            if not order['returned']:
                doc_id = order['order_document']
                student = get_object_or_404(Student, pk=doc_id)
                order['ordered_name'] = "%s, %s" % (student.last_name, student.first_name)
                return_list.append(order)

        return content_response(return_list, status_code=status.HTTP_200_OK)

    @staticmethod
    def retrieve(request, student_order_id):
        log_request_information(request, "basic")
        student_order = get_object_or_404(StudentOrder, pk=student_order_id)

        return content_response(student_order, status_code=status.HTTP_200_OK)

    def return_order(self, request, student_order_id):
        log_request_information(request, "basic")

        student_order = self.student_order_dao.return_order(request, student_order_id)

        return content_response(model_to_dict(student_order), status_code=status.HTTP_200_OK)

    def pending_order(self, request, student_order_id):
        log_request_information(request, "basic")

        student_order = self.student_order_dao.pending_order(request, student_order_id)

        return content_response(model_to_dict(student_order), status_code=status.HTTP_200_OK)

    def create_order(self, request):
        log_request_information(request, "basic")

        student_order = self.student_order_dao.create_student_order(request)

        return content_response(model_to_dict(student_order), status_code=status.HTTP_201_CREATED)
