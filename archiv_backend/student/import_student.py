import json
import os
from getpass import getpass

import requests
import sys
from requests.auth import HTTPBasicAuth

os.environ['DJANGO_SETTINGS_MODULE'] = 'backend.settings'

import unicodecsv as csv
import archiv_backend.keys as k
from datetime import datetime, timedelta


__author__ = 'paullichtenberger'

args = sys.argv

filename = args[1]
username = args[2]

user = requests.get('http://localhost:8000/login', auth=HTTPBasicAuth(username, getpass()))

user = user.json()

token = user['login_token']

print os.path.realpath(__file__)

exception_file = open("failed.csv", "a")
fail_counter = 0

with open(filename, 'rU') as data_file:
    headers = {'Authorization': 'Token %s' % str(token),
               'Content-Type': 'application/json',
               'Accept':'application/json'}
    reader = csv.reader(data_file, delimiter=',', encoding='utf-8')
    writer = csv.writer(exception_file, delimiter=',', encoding='utf-8')
    for row in reader:
        try:
            try:
                date_of_birth = (datetime.strptime(row[6], '%d.%m.%y') - timedelta(days=36500)).strftime('%Y-%m-%d')
            except Exception as e:
                date_of_birth = None
                try:
                    date_of_birth = datetime.strptime(row[6], '%d.%m.%Y')
                except Exception as e:
                    date_of_birth = None

            student = {
                k.ARCHIVE_NUMBER: row[0],
                k.BOX_NUMBER: row[1],
                k.STUDENT_NUMBER: row[2],
                k.LAST_NAME: row[3],
                k.FIRST_NAME: row[4],
                k.NAME_OF_BIRTH: row[5],
                k.IMMATRICULATION: row[7],
                k.EXMATRICULATION: row[8],
                k.FILE_NUMBER: row[9],
                k.DATE_OF_FILING:row[10],
                k.ANNOTATIONS:row[11],
                k.ALSO_CONTAINS:row[12],
                k.BOX_NUMBER_OLD:row[13]
            }

            if date_of_birth:
                student[k.DATE_OF_BIRTH] = date_of_birth

            res = requests.post('http://localhost:8000/students', data=json.dumps(student), headers=headers)

            if res.status_code != 201:
                fail_counter += 1
                writer.writerow(row)

        except Exception as ex:
            print(ex.message)
            fail_counter += 1
            writer.writerow(row)

    print "%s documents failed to post, see failed.csv for rows" % str(fail_counter)
