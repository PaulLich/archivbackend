import operator
from datetime import datetime

from django.db.models import Q
from django.shortcuts import get_object_or_404

from archiv_backend import exceptions, error_codes
from archiv_backend.base_dao import BaseDAO
from models import Student, StudentOrder, StudentFTS
import archiv_backend.keys as k


class StudentDAO(BaseDAO):

    def __init__(self):
        pass

    @staticmethod
    def create_or_update_student(payload, this_id=None):

        if this_id:
            student = Student.objects.get(pk=int(this_id))
        else:
            student = Student()

        student.archive_number = payload.get(k.ARCHIVE_NUMBER)
        student.box_number = payload.get(k.BOX_NUMBER)
        student.box_number_old = payload.get(k.BOX_NUMBER_OLD)
        student.file_number = payload.get(k.FILE_NUMBER)
        student.first_name = payload.get(k.FIRST_NAME)
        student.last_name = payload.get(k.LAST_NAME)
        student.student_number = payload.get(k.STUDENT_NUMBER)
        student.name_of_birth = payload.get(k.NAME_OF_BIRTH)
        student.immatriculation = payload.get(k.IMMATRICULATION)
        student.exmatriculation = payload.get(k.EXMATRICULATION)
        student.date_of_filing = payload.get(k.DATE_OF_FILING)
        student.annotations = payload.get(k.ANNOTATIONS)
        student.also_contains = payload.get(k.ALSO_CONTAINS)

        if payload.get(k.DATE_OF_BIRTH):
            student.date_of_birth = payload.get(k.DATE_OF_BIRTH)

        student.save()

        if this_id:
            student_fts = StudentFTS.objects.filter(student=student).first()
            if student_fts is None:
                student_fts = StudentFTS()

        else:
            student_fts = StudentFTS()

        student_fts.student = student
        student_fts.content = u'\n'.join((unicode(x) for x in filter(None, (student.archive_number,
                                                                            student.box_number,
                                                                            student.box_number_old,
                                                                            student.file_number,
                                                                            student.first_name,
                                                                            student.last_name,
                                                                            student.student_number,
                                                                            student.name_of_birth,
                                                                            student.date_of_birth,
                                                                            student.immatriculation,
                                                                            student.exmatriculation,
                                                                            student.date_of_filing,
                                                                            student.annotations,
                                                                            student.also_contains,
                                                                            ))))

        student_fts.save()

        return student

    def perform_fts(self, request):
        query_string = request.query_params.get('search_term')
        if query_string == u'*':
            return self.prepare_list_response(list(Student.objects.all()))
        if query_string is not None and query_string is not u'':
            query_list = query_string.split()

            received_objects = StudentFTS.objects.filter(
                reduce(operator.and_, (Q(content__icontains=x) for x in query_list))).select_related('student')

            student_list = []
            for e in received_objects:
                student_list.append(e.student)

            prepared_list = self.prepare_list_response(student_list)

            return prepared_list
        else:
            return []


class StudentOrderDAO(BaseDAO):

    def __init__(self):
        pass

    @staticmethod
    def create_student_order(request):

        student_order = StudentOrder()
        payload = request.data

        doc_id = payload.get(k.ORDER_DOCUMENT)

        pending_orders = StudentOrder.objects.filter(returned__exact=False)
        pending_orders = pending_orders.filter(order_document_id__exact=doc_id)

        for order in pending_orders:
            if not order.returned:
                raise exceptions.ValidationError("Document is not returned yet",
                                                 error_code=error_codes.DOCUMENT_IS_NOT_RETURNED)

        student_order.order_document = get_object_or_404(Student, pk=payload.get(k.ORDER_DOCUMENT))
        student_order.user_id = request.user.id
        student_order.user_first_name = request.user.first_name
        student_order.user_last_name = request.user.last_name
        student_order.user_username = request.user.username
        student_order.order_date = datetime.now()
        student_order.returned = False
        student_order.pending = True

        student_order.save()

        return student_order

    def return_order(self, request, student_order_id):
        student_order = get_object_or_404(StudentOrder, pk=student_order_id)

        student_order.returned = True
        student_order.return_date = datetime.now()

        student_order.save()
        return student_order

    def pending_order(self, request, student_order_id):
        student_order = get_object_or_404(StudentOrder, pk=student_order_id)

        student_order.pending = False

        student_order.save()
        return student_order
