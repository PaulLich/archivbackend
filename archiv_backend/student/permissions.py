from rest_framework import permissions

class StudentPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.auth

        if "student" != user["user_role"] and "admin" != user["user_role"]:
            return False

        return True
