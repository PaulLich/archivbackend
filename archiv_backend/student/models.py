from django.db import models


class Student(models.Model):

    archive_number = models.PositiveIntegerField(
        default=0
    )
    box_number = models.PositiveIntegerField(
        null=True,
        blank=True
    )
    box_number_old = models.PositiveIntegerField(
        null=True,
        blank=True
    )
    file_number = models.PositiveIntegerField(
        null=True,
        blank=True
    )
    first_name = models.CharField(
        max_length=255,
        default=''
    )
    last_name = models.CharField(
        max_length=255,
        default=''
    )
    student_number = models.PositiveIntegerField(
        null=True,
        blank=True,
    )
    name_of_birth = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    date_of_birth = models.DateField(
        null=True,
        blank=True
    )
    immatriculation = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    exmatriculation = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    date_of_filing = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    annotations = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    also_contains = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )

    def __str__(self):

        values = [
            self.archive_number,
            self.box_number,
            self.box_number_old,
            self.file_number,
            str(self.first_name).decode('utf-8'),
            str(self.last_name).decode('utf-8'),
            self.student_number,
            str(self.name_of_birth).decode('utf-8'),
            str(self.date_of_birth).decode('utf-8'),
            str(self.place_of_birth).decode('utf-8'),
            str(self.immatriculation).decode('utf-8'),
            str(self.exmatriculation).decode('utf-8'),
            str(self.date_of_filing).decode('utf-8'),
            str(self.annotations).decode('utf-8'),
            str(self.also_contains).decode('utf-8'),
            self.id
        ]

        return ' '.join(str(v) for v in values)


class StudentOrder(models.Model):

    order_document = models.ForeignKey(
        Student,
    )

    order_date = models.DateField()

    user_username = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_last_name = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_first_name = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_id = models.IntegerField(
        null=False
    )

    return_date = models.DateField(
        null=True,
        blank=True
    )

    returned = models.BooleanField(
        default=False
    )

    pending = models.BooleanField(
        default=True
    )

    def __str__(self):

        values = [
            self.order_document,
            self.order_date,
            str(self.return_date).decode('utf-8'),
            self.returned,
        ]

        return ' '.join(str(v) for v in values)


class StudentFTS(models.Model):

    student = models.ForeignKey(
        Student,
    )

    content = models.TextField()

    def __repr__(self):
        return str(self.content).decode('utf-8')

    def __str__(self):
        return str(self.content).decode('utf-8')
