import struct
from datetime import datetime

from django.db.models import Q
from django.forms import model_to_dict

import archiv_backend.keys as k


class BaseDAO:

    def __init__(self):
        pass

    @staticmethod
    def prepare_list_response(obj):
        input_list = list(obj)

        return_list = []

        for e in input_list:
            entry = model_to_dict(e)
            entry[k.ID] = e.id
            return_list.append(entry)

        return return_list

    @staticmethod
    def build_query(params):

        qterms = Q(first_name__isnull=True)
        # Noch unschoen geloest. Ist noetig, da wir ein initiales q object brauchen an das wir andere anhaengen koennen

        if k.ARCHIVE_NUMBER in params:
            qterms |= Q(student_number=int(params[k.ARCHIVE_NUMBER]))

        if k.ACADEMIC_TITLE in params:
            qterms |= Q(academic_title__icontains=str(params[k.ACADEMIC_TITLE]))

        if k.ALSO_CONTAINS in params:
            qterms |= Q(also_contains__icontains=str(params[k.ALSO_CONTAINS]))

        if k.ANNOTATIONS in params:
            qterms |= Q(annotations__icontains=str(params[k.ANNOTATIONS]))

        if k.BEGIN_OF_STUDIES in params:
            qterms |= Q(begin_of_studies__icontains=str(params[k.BEGIN_OF_STUDIES]))

        if k.BOX_NUMBER in params:
            qterms |= Q(box_number=int(params[k.BOX_NUMBER]))

        if k.BOX_NUMBER_OLD in params:
            qterms |= Q(box_number_old=int(params[k.BOX_NUMBER_OLD]))

        if k.DATE_OF_BIRTH in params:
            qterms |= Q(date_of_birth=datetime.strptime(params[k.DATE_OF_BIRTH], '%Y-%m-%d'))

        if k.DATE_OF_DEATH in params:
            qterms |= Q(date_of_death=datetime.strptime(params[k.DATE_OF_DEATH], '%Y-%m-%d'))

        if k.DATE_OF_FILING in params:
            qterms |= Q(date_of_filing=datetime.strptime(params[k.DATE_OF_FILING], '%Y-%m-%d'))

        if k.DEGREE_SUBJECT in params:
            qterms |= Q(degree_subject__icontains=str(params[k.DEGREE_SUBJECT]))

        if k.DEPARTMENT in params:
            qterms |= Q(department__icontains=str(params[k.DEPARTMENT]))

        if k.END_OF_EMPLOYMENT in params:
            qterms |= Q(end_of_employment=datetime.strptime(params[k.END_OF_EMPLOYMENT], '%Y-%m-%d'))

        if k.END_OF_STUDIES in params:
            qterms |= Q(end_of_studies__icontains=str(params[k.END_OF_STUDIES]))

        if k.EXMATRICULATION in params:
            qterms |= Q(exmatriculation__icontains=str(params[k.EXMATRICULATION]))

        if k.FILE_NUMBER in params:
            qterms |= Q(file_number=int(params[k.FILE_NUMBER]))

        if k.FIRST_NAME in params:
            qterms |= Q(first_name__icontains=str(params[k.FIRST_NAME]))

        if k.ID in params:
            qterms |= Q(pk=int(params[k.ID]))

        if k.IMMATRICULATION in params:
            qterms |= Q(immatriculation__icontains=str(params[k.IMMATRICULATION]))

        if k.JOB_TITLE in params:
            qterms |= Q(job_title__icontains=str(params[k.JOB_TITLE]))

        if k.LAST_NAME in params:
            qterms |= Q(last_name__icontains=str(params[k.LAST_NAME]))

        if k.MAIN_SUBJECT in params:
            qterms |= Q(main_subject__icontains=str(params[k.MAIN_SUBJECT]))

        if k.NAME_OF_BIRTH in params:
            qterms |= Q(name_of_birth__icontains=str(params[k.NAME_OF_BIRTH]))

        if k.ORDER_DATE in params:
            qterms |= Q(order_date=datetime.strptime(params[k.ORDER_DATE], '%Y-%m-%d'))

        if k.PLACE_OF_BIRTH in params:
            qterms |= Q(place_of_birth__icontains=str(params[k.PLACE_OF_BIRTH]))

        if k.RETURN_DATE in params:
            qterms |= Q(return_date=datetime.strptime(params[k.RETURN_DATE], '%Y-%m-%d'))

        if k.RETURNED in params:
            qterms |= Q(returned=bool(params[k.RETURNED]))

        if k.SECONDARY_SUBJECT in params:
            qterms |= Q(secondary_subject__icontains=str(params[k.SECONDARY_SUBJECT]))

        if k.START_OF_EMPLOYMENT in params:
            qterms |= Q(start_of_employment=datetime.strptime(params[k.START_OF_EMPLOYMENT], '%Y-%m-%d'))

        if k.STUDENT_NUMBER in params:
            qterms |= Q(student_number=int(params[k.STUDENT_NUMBER]))

        if k.TITLE in params:
            qterms |= Q(title__icontains=str(params[k.TITLE]))

        return qterms

    @staticmethod
    def get_search_params(request):
        params = {}

        for p in request.query_params:
            params[p] = request.query_params.get(p)

        return params

    def get_by_search_fields(self, cls, params):

        qterms = BaseDAO.build_query(params)

        received_objects = cls.objects.filter(qterms)

        prepared_list = self.prepare_list_response(received_objects)

        return self.prepare_doc_count(prepared_list, params)

    def get_all_by_request(self, cls):
        received_objects = cls.objects.all()

        prepared_list = self.prepare_list_response(received_objects)

        return prepared_list

    @staticmethod
    def prepare_doc_count(obj, params):

        for e in obj:
            count = 0
            for p in params:
                # vergleiche strings
                if p in [k.FIRST_NAME, k.LAST_NAME, k.TITLE, k.NAME_OF_BIRTH, k.PLACE_OF_BIRTH, k.MAIN_SUBJECT,
                         k.SECONDARY_SUBJECT, k.DEGREE_SUBJECT, k.ACADEMIC_TITLE, k.IMMATRICULATION, k.EXMATRICULATION,
                         k.DATE_OF_FILING, k.ALSO_CONTAINS, k.ANNOTATIONS, k.JOB_TITLE]:
                    str1 = params[p].lower()
                    str2 = e.get(p).lower()
                    if str1 in str2:
                        count += 1
                # vegleiche ints
                elif p in [k.STUDENT_NUMBER, k.BEGIN_OF_STUDIES, k.END_OF_STUDIES, ]:
                    if int(params[p]) == int(e.get(p)):
                        count += 1
                # vergleiche datum
                elif p in [k.DATE_OF_BIRTH, k.DATE_OF_DEATH, k.START_OF_EMPLOYMENT, k.END_OF_EMPLOYMENT]:
                    if datetime.strptime(params[p], '%Y-%m-%d') == e.get(p):
                        count += 1

            e['count'] = count

        return sorted(obj, key=lambda c: c.get('count'), reverse=True)

    @staticmethod
    def _parse_match_info(buf):
        bufsize = len(buf)  # Length in bytes.
        return [struct.unpack('@I', buf[i:i+4])[0] for i in range(0, bufsize, 4)]

    def delete_entry(self, cls, entry_id):
        return cls.objects.get(pk=int(entry_id)).delete()
