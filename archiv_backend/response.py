from rest_framework import status
from rest_framework.response import Response


def get_headers():

    headers = {'Access-Control-Allow-Origin': '*'}

    return headers


def content_response(response_obj, status_code=status.HTTP_200_OK,
                     obj_modification_function=None, default='{}'):

    if response_obj is None:
        return default

    if obj_modification_function:
        response_obj = map_if_list(obj_modification_function, response_obj)

    return Response(response_obj, status=status_code, headers=get_headers())


def string_response(msg, status_code=status.HTTP_200_OK, prefix="Success: ", default_msg=""):
    resp_text = prefix + (msg.capitalize() or default_msg)
    return Response(resp_text, status=status_code, headers=get_headers())


def map_if_list(funcs, value):
    if not isinstance(funcs, list):
        funcs = [funcs]

    for func in funcs:
        if isinstance(value, list):
            value = map(func, value)
        else:
            value = func(value)

    return value
