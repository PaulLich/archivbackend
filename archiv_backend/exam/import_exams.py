import json
import os

import requests
from requests.auth import HTTPBasicAuth

os.environ['DJANGO_SETTINGS_MODULE'] = 'backend.settings'

import unicodecsv as csv
import archiv_backend.keys as k
from datetime import datetime
import sys
from getpass import getpass


__author__ = 'paullichtenberger'

args = sys.argv

filename = args[1]
username = args[2]

user = requests.get('http://localhost:8000/login', auth=HTTPBasicAuth(username, getpass()))

user = user.json()

token = user['login_token']

exception_file = open("failed.csv", "a")
fail_counter = 0

print os.path.realpath(__file__)

with open(str(filename), 'rU') as data_file:
    headers = {'Authorization': 'Token %s' % str(token),
               'Content-Type': 'application/json',
               'Accept': 'application/json'}
    writer = csv.writer(exception_file, delimiter=',', encoding='utf-8')
    reader = csv.reader(data_file, delimiter=',', encoding='utf-8')
    for row in reader:
        print(row)
        try:
            exam = {
                k.SIGNATURE: row[0],
                k.LAST_NAME: row[1],
                k.FIRST_NAME: row[2],
                k.TITLE: row[3],
                k.DATE_OF_BIRTH: datetime.strptime(row[4], '%d.%m.%Y').strftime('%Y-%m-%d'),
                k.PLACE_OF_BIRTH: row[5],
                k.NAME_OF_BIRTH: row[6],
                k.STUDENT_NUMBER: row[7],
                k.BEGIN_OF_STUDIES: row[8],
                k.END_OF_STUDIES: row[9],
                k.DEPARTMENT: row[10],
                k.SECONDARY_SUBJECT: row[11],
                k.MAIN_SUBJECT: row[12],
                k.DEGREE_SUBJECT: row[13],
                k.ACADEMIC_TITLE: row[14]
            }

            print(exam)

            res = requests.post('http://localhost:8000/exams', data=json.dumps(exam), headers=headers)

            if res.status_code != 201:
                fail_counter += 1
                writer.writerow(row)

        except Exception as ex:
            print(ex.message)
            fail_counter += 1
            writer.writerow(row)

    print "%s documents failed to post, see failed.csv for rows" % str(fail_counter)



