import logging

from rest_framework import permissions

logger = logging.getLogger(__name__)


class ExamPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.auth

        # Denies Permission to all but admins and exam users
        if "exam" != user["user_role"] and "admin" != user["user_role"]:
            return False

        return True
