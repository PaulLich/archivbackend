from django.db import models
from peewee import *

from archiv_backend.archive_users.models import ArchivUser


class Exam(models.Model):

    archive_number = models.PositiveIntegerField(
        default=0,
        blank=True,
        null=True
    )
    signature = models.PositiveIntegerField(
        default=0,
    )
    first_name = models.CharField(
        max_length=255,
        default='',
    )
    last_name = models.CharField(
        max_length=255,
        default='',
    )
    title = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    name_of_birth = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    date_of_birth = models.DateField(
        null=True,
        blank=True
    )
    place_of_birth = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    student_number = models.PositiveIntegerField(
        blank=True,
        null=True,
    )
    begin_of_studies = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    end_of_studies = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    department = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    main_subject = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    secondary_subject = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    degree_subject = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    academic_title = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )

    def __str__(self):

        values = [
            self.archive_number,
            str(self.first_name).decode('utf-8'),
            str(self.last_name).decode('utf-8'),
            str(self.title).decode('utf-8'),
            str(self.name_of_birth).decode('utf-8'),
            str(self.date_of_birth).decode('utf-8'),
            str(self.place_of_birth).decode('utf-8'),
            self.student_number,
            str(self.begin_of_studies).decode('utf-8'),
            str(self.end_of_studies).decode('utf-8'),
            str(self.department).decode('utf-8'),
            str(self.main_subject).decode('utf-8'),
            str(self.secondary_subject).decode('utf-8'),
            str(self.degree_subject).decode('utf-8'),
            str(self.academic_title).decode('utf-8'),
            self.id
        ]

        return ' '.join(str(v) for v in values)

    def __repr__(self):
        values = [
            self.archive_number,
            str(self.first_name).decode('utf-8'),
            str(self.last_name).decode('utf-8'),
            self.id
        ]

        return ' '.join(str(v) for v in values)


class ExamOrder(models.Model):

    order_document = models.ForeignKey(
        Exam,
    )

    order_date = models.DateField()

    user_username = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_last_name = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_first_name = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_id = models.IntegerField(
        null=False
    )

    return_date = models.DateField(
        null=True,
        blank=True
    )

    returned = models.BooleanField(
        default=False
    )

    pending = models.BooleanField(
        default=True
    )

    def __str__(self):

        values = [
            self.order_document,
            self.order_date,
            str(self.return_date).decode('utf-8'),
            self.returned,
        ]

        return ' '.join(str(v) for v in values)

    def __repr__(self):

        values = [
            self.order_document,
            str(self.order_date).decode('utf-8'),
            str(self.return_date).decode('utf-8'),
            self.returned,
        ]

        return ' '.join(str(v) for v in values)


class ExamFTS(models.Model):

    exam = models.ForeignKey(
        Exam,
    )

    content = models.TextField()

    def __repr__(self):
        return str(self.id)

    def __str__(self):
        return str(self.id)
