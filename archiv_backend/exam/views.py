import logging

from django.forms import model_to_dict
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated

from archiv_backend.authentication import log_request_information, TokenAuth
from archiv_backend.exam.models import Exam, ExamOrder
from archiv_backend.response import content_response, string_response
from dao import ExamDAO, ExamOrderDAO

logger = logging.getLogger(__name__)


class ExamSet(viewsets.ViewSet):

    authentication_classes = (TokenAuth,)
    permission_classes = (IsAuthenticated,)

    def __init__(self, **kwargs):
        super(ExamSet, self).__init__(**kwargs)
        self.exam_dao = ExamDAO()

    def get(self, request):
        log_request_information(request, "basic")
        params = self.exam_dao.get_search_params(request)

        if not params:
            exams = self.exam_dao.get_all_by_request(Exam)
        else:
            exams = self.exam_dao.get_by_search_fields(Exam, params)

        logger.info("Successfully retrieved %s exams" % len(exams))
        return content_response(exams, status_code=status.HTTP_200_OK)

    def put(self, request, exam_id):
        log_request_information(request, "basic")

        payload = request.data

        exam = self.exam_dao.create_or_update_exam(payload, exam_id)

        logger.info("Successfully updated exam %s" % exam_id)
        return content_response(model_to_dict(exam), status_code=status.HTTP_200_OK)

    def post(self, request):
        log_request_information(request, "basic")

        payload = request.data

        exam = self.exam_dao.create_or_update_exam(payload)

        logger.info("Successfully created new exam %s" % exam.id)
        return content_response(model_to_dict(exam), status_code=status.HTTP_201_CREATED)

    def retrieve(self, request, exam_id):
        log_request_information(request, "basic")
        exam = get_object_or_404(Exam, pk=int(exam_id))

        logger.info("Successfully retrieved exam %s" % exam_id)
        return content_response(model_to_dict(exam), status_code=status.HTTP_200_OK)

    def search(self, request):
        log_request_information(request, "basic")
        exams = self.exam_dao.perform_fts(request)

        logger.info("Successfully fetched %s exam documents" % len(exams))
        return content_response(exams, status_code=status.HTTP_200_OK)

    def delete(self, request, exam_id):
        log_request_information(request, "basic")
        self.exam_dao.delete_entry(Exam, exam_id)

        logger.info("Successfully deleted exam %s" % exam_id)
        return string_response("Successfully deleted exam %s" % exam_id, status_code=status.HTTP_200_OK)


class ExamOrderSet(viewsets.ViewSet):

    authentication_classes = (TokenAuth,)
    permission_classes = (IsAuthenticated,)

    def __init__(self, **kwargs):
        super(ExamOrderSet, self).__init__(**kwargs)
        self.exam_order_dao = ExamOrderDAO()

    def get(self, request):
        log_request_information(request, "basic")
        params = self.exam_order_dao.get_search_params(request)

        return_list = []

        if not params:
            exam_order = self.exam_order_dao.get_all_by_request(ExamOrder)
        else:
            exam_order = self.exam_order_dao.get_by_search_fields(ExamOrder, params)

        for order in exam_order:
            if not order['returned']:
                doc_id = order['order_document']
                exam = get_object_or_404(Exam, pk=doc_id)
                order['ordered_name'] = "%s, %s" % (exam.last_name, exam.first_name)
                return_list.append(order)

        logger.info("Successfully fetched %s exam order documents" % len(return_list))
        return content_response(return_list, status_code=status.HTTP_200_OK)

    def retrieve(self, request, exam_order_id):
        log_request_information(request, "basic")
        exam_order = get_object_or_404(ExamOrder, pk=exam_order_id)

        return content_response(model_to_dict(exam_order), status_code=status.HTTP_200_OK)

    def return_order(self, request, exam_order_id):
        log_request_information(request, "basic")

        exam_order = self.exam_order_dao.return_order(exam_order_id)

        return content_response(model_to_dict(exam_order), status_code=status.HTTP_200_OK)

    def pending_order(self, request, exam_order_id):
        log_request_information(request, "basic")

        exam_order = self.exam_order_dao.pending_order(exam_order_id)

        return content_response(model_to_dict(exam_order), status_code=status.HTTP_200_OK)

    def create_order(self, request):
        log_request_information(request, "basic")

        exam_order = self.exam_order_dao.create_exam_order(request)

        return content_response(model_to_dict(exam_order), status_code=status.HTTP_201_CREATED)
