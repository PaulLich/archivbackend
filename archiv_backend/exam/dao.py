import logging
import operator
from datetime import datetime

from django.db.models import Q
from django.shortcuts import get_object_or_404

import archiv_backend.keys as k
from archiv_backend import exceptions, error_codes
from archiv_backend.base_dao import BaseDAO
from archiv_backend.exam.models import Exam, ExamOrder, ExamFTS

logger = logging.getLogger(__name__)


class ExamDAO(BaseDAO):
    def __init__(self):
        pass

    # Creates or updates an exam document
    @staticmethod
    def create_or_update_exam(payload, this_id=None):

        if this_id:
            logger.info("Update exam %s with payload %s" % (this_id, payload))
            exam = Exam.objects.get(pk=int(this_id))
        else:
            logger.info("Create new exam with payload %s" % payload)
            exam = Exam()
            
        exam.archive_number = payload.get(k.ARCHIVE_NUMBER)
        exam.signature = payload.get(k.SIGNATURE)
        exam.first_name = payload.get(k.FIRST_NAME)
        exam.last_name = payload.get(k.LAST_NAME)
        exam.title = payload.get(k.TITLE)
        exam.name_of_birth = payload.get(k.NAME_OF_BIRTH)
        exam.date_of_birth = payload.get(k.DATE_OF_BIRTH)
        exam.place_of_birth = payload.get(k.PLACE_OF_BIRTH)
        exam.student_number = payload.get(k.STUDENT_NUMBER)
        exam.begin_of_studies = payload.get(k.BEGIN_OF_STUDIES)
        exam.end_of_studies = payload.get(k.END_OF_STUDIES)
        exam.main_subject = payload.get(k.MAIN_SUBJECT)
        exam.secondary_subject = payload.get(k.SECONDARY_SUBJECT)
        exam.degree_subject = payload.get(k.DEGREE_SUBJECT)
        exam.academic_title = payload.get(k.ACADEMIC_TITLE)

        exam.save()

        # Write free text search index column
        if this_id:
            exam_fts = ExamFTS.objects.filter(exam=exam).first()
            if exam_fts is None:
                exam_fts = ExamFTS()
        else:
            exam_fts = ExamFTS()

        exam_fts.exam = exam

        exam_fts.content = u'\n'.join((unicode(x) for x in filter(None, (exam.archive_number,
                                                                         exam.signature,
                                                                         exam.first_name,
                                                                         exam.last_name,
                                                                         exam.title,
                                                                         exam.name_of_birth,
                                                                         exam.date_of_birth,
                                                                         exam.place_of_birth,
                                                                         exam.begin_of_studies,
                                                                         exam.end_of_studies,
                                                                         exam.main_subject,
                                                                         exam.secondary_subject,
                                                                         exam.degree_subject,
                                                                         exam.academic_title
                                                                         ))))

        exam_fts.save()

        return exam

    # Perform a free text search on exams
    def perform_fts(self, request):
        query_string = request.query_params.get('search_term')
        if query_string == u'*':
            return self.prepare_list_response(list(Exam.objects.all()))
        elif query_string is not None and query_string is not u'':

            query_list = query_string.split()

            received_objects = ExamFTS.objects.filter(
                reduce(operator.and_, (Q(content__icontains=x) for x in query_list))).select_related('exam')

            exam_list = []
            for e in received_objects:
                exam_list.append(e.exam)

            prepared_list = self.prepare_list_response(exam_list)

            return prepared_list
        else:
            return []


class ExamOrderDAO(BaseDAO):

    def __init__(self):
        pass

    # Creates an exam order document
    @staticmethod
    def create_exam_order(request):

        exam_order = ExamOrder()
        payload = request.data

        doc_id = payload.get(k.ORDER_DOCUMENT)

        pending_orders = ExamOrder.objects.filter(returned__exact=False)
        pending_orders = pending_orders.filter(order_document_id__exact=doc_id)

        for order in pending_orders:
            if not order.returned:
                logger.error("Tried to order document which is not returned yet doc_id: %s" % doc_id)
                raise exceptions.ValidationError("Document is not returned yet",
                                                 error_code=error_codes.DOCUMENT_IS_NOT_RETURNED)

        exam_order.order_document = get_object_or_404(Exam, pk=payload.get(k.ORDER_DOCUMENT))
        exam_order.user_id = request.user.id
        exam_order.user_first_name = request.user.first_name
        exam_order.user_last_name = request.user.last_name
        exam_order.user_username = request.user.username
        exam_order.order_date = datetime.now()
        exam_order.returned = False
        exam_order.pending = True

        exam_order.save()

        return exam_order

    # Sets exam order document to returned
    @staticmethod
    def return_order(exam_order_id):
        exam_order = get_object_or_404(ExamOrder, pk=exam_order_id)

        exam_order.returned = True
        exam_order.return_date = datetime.now()

        exam_order.save()
        return exam_order

    # Sets exam order document to pending
    @staticmethod
    def pending_order(exam_order_id):
        exam_order = get_object_or_404(ExamOrder, pk=exam_order_id)

        exam_order.pending = False

        exam_order.save()
        return exam_order
