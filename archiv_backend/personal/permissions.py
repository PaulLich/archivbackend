from rest_framework import permissions


class PersonalPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.auth

        # Restricts permission to admins and personal users
        if "personal" != user["user_role"] and "admin" != user["user_role"]:
            return False

        return True
