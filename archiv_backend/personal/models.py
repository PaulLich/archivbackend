from django.db import models


class Personal(models.Model):

    archive_number = models.PositiveIntegerField(
        default=0,
    )
    first_name = models.CharField(
        max_length=255,
        default='',
    )
    last_name = models.CharField(
        max_length=255,
        default='',
    )
    title = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    name_of_birth = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    date_of_birth = models.DateField(
        null=True,
        blank=True
    )
    place_of_birth = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    date_of_death = models.DateField(
        null=True,
        blank=True
    )
    start_of_employment = models.DateField(
        null=True,
        blank=True
    )
    end_of_employment = models.DateField(
        null=True,
        blank=True
    )
    job_title = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )

    def __str__(self):

        values = [
            self.archive_number,
            str(self.first_name).decode('utf-8'),
            str(self.last_name).decode('utf-8'),
            str(self.title).decode('utf-8'),
            str(self.name_of_birth).decode('utf-8'),
            str(self.date_of_birth),
            str(self.place_of_birth).decode('utf-8'),
            str(self.date_of_death),
            str(self.start_of_employment).decode('utf-8'),
            str(self.end_of_employment).decode('utf-8'),
            str(self.job_title).decode('utf-8'),
            self.id,
        ]

        return ' '.join(str(v) for v in values)


class PersonalOrder(models.Model):

    order_document = models.ForeignKey(
        Personal,
    )

    order_date = models.DateField()

    user_username = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_last_name = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_first_name = models.TextField(
        max_length=255,
        null=False,
        blank=False
    )

    user_id = models.IntegerField(
        null=False
    )

    return_date = models.DateField(
        null=True,
        blank=True
    )

    returned = models.BooleanField(
        default=False
    )

    pending = models.BooleanField(
        default=True
    )

    def __str__(self):

        values = [
            self.order_document,
            self.order_date,
            str(self.return_date).decode('utf-8'),
            self.returned,
        ]

        return ' '.join(str(v) for v in values)


class PersonalFTS(models.Model):

    personal = models.ForeignKey(
        Personal,
    )

    content = models.TextField()

    def __repr__(self):
        return str(self.content).decode('utf-8')

    def __str__(self):
        return str(self.content).decode('utf-8')
