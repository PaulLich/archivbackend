import json
import os
from getpass import getpass

import requests
import sys
from requests.auth import HTTPBasicAuth

os.environ['DJANGO_SETTINGS_MODULE'] = 'backend.settings'

import unicodecsv as csv
import archiv_backend.keys as k
from datetime import datetime, timedelta


__author__ = 'paullichtenberger'

args = sys.argv

filename = args[1]
username = args[2]

user = requests.get('http://localhost:8000/login', auth=HTTPBasicAuth(username, getpass()))

user = user.json()

token = user['login_token']

exception_file = open("failed.csv", "a")
fail_counter = 0


print os.path.realpath(__file__)

with open(filename, 'rU') as data_file:
    headers = {'Authorization': 'Token %s' % str(token),
               'Content-Type': 'application/json',
               'Accept':'application/json'}
    writer = csv.writer(exception_file, delimiter=',', encoding='utf-8')
    reader = csv.reader(data_file, delimiter=',', encoding='utf-8')
    for row in reader:
        print(row)
        try:
            try:
                date_of_birth = (datetime.strptime(row[4], '%d.%m.%y') - timedelta(days=36500)).strftime('%Y-%m-%d')
            except Exception as e:
                date_of_birth = None

            try:
                date_of_death = (datetime.strptime(row[6], '%d.%m.%y') - timedelta(days=36500)).strftime('%Y-%m-%d')
            except Exception as e:
                date_of_death = None

            try:
                start = datetime.strptime(str(row[8]).strip(" ")[:4], '%Y').strftime('%Y-01-01')
            except Exception as e:
                start = None

            try:
                end = datetime.strptime(str(row[9]).strip(" ")[:4], '%Y').strftime('%Y-01-01')
            except Exception as e:
                end = None

            personal = {
                k.ARCHIVE_NUMBER: row[0],
                k.LAST_NAME: row[1],
                k.FIRST_NAME: row[2],
                k.TITLE: row[3],
                k.PLACE_OF_BIRTH: row[5],
                k.NAME_OF_BIRTH: row[7],
                k.JOB_TITLE: row[10]
            }

            if date_of_birth:
                personal[k.DATE_OF_BIRTH] = date_of_birth

            if date_of_death:
                personal[k.DATE_OF_DEATH] = date_of_death

            if start:
                personal[k.START_OF_EMPLOYMENT] = start

            if end:
                personal[k.END_OF_EMPLOYMENT] = end

            print(personal)

            res = requests.post('http://localhost:8000/personal', data=json.dumps(personal), headers=headers)

            if res.status_code != 201:
                fail_counter += 1
                writer.writerow(row)

        except Exception as ex:
            fail_counter += 1
            writer.writerow(row)

    print "%s documents failed to post, see failed.csv for rows" % str(fail_counter)



