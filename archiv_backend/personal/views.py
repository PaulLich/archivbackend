import logging

from django.forms import model_to_dict
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated

from archiv_backend.authentication import log_request_information, TokenAuth
from archiv_backend.personal.models import Personal
from archiv_backend.personal.models import PersonalOrder
from archiv_backend.response import content_response, string_response
from dao import PersonalDAO, PersonalOrderDAO

logger = logging.getLogger(__name__)


class PersonalSet(viewsets.ViewSet):

    authentication_classes = (TokenAuth,)
    permission_classes = (IsAuthenticated,)

    def __init__(self, **kwargs):
        super(PersonalSet, self).__init__(**kwargs)
        self.personal_dao = PersonalDAO()

    def get(self, request):
        log_request_information(request, "basic")

        params = self.personal_dao.get_search_params(request)

        if not params:
            persons = self.personal_dao.get_all_by_request(Personal)
        else:
            persons = self.personal_dao.get_by_search_fields(Personal, params)

        logger.info("Successfully fetched %s personal docs" % len(persons))
        return content_response(persons, status_code=status.HTTP_200_OK)

    def post(self, request):
        log_request_information(request, "basic")

        payload = request.data

        personal = self.personal_dao.create_or_update_personal(payload)

        logger.info("Successfully created personal doc %s" % personal.id)
        return content_response(model_to_dict(personal), status_code=status.HTTP_201_CREATED)

    def put(self, request, personal_id):
        log_request_information(request, "basic")

        payload = request.data

        personal = self.personal_dao.create_or_update_personal(payload, personal_id)

        logger.info("Successfully updated personal doc %s" % personal_id)
        return content_response(model_to_dict(personal), status_code=status.HTTP_201_CREATED)

    @staticmethod
    def retrieve(request, personal_id):
        log_request_information(request, "basic")
        personal = get_object_or_404(Personal, pk=int(personal_id))

        logger.info("Successfully retrieved personal doc %s" % personal_id)
        return content_response(model_to_dict(personal), status_code=status.HTTP_200_OK)

    def search(self, request):
        log_request_information(request, "basic")
        personal = self.personal_dao.perform_fts(request)

        logger.info("Successfully fetched %s personal docs" % len(personal))
        return content_response(personal, status_code=status.HTTP_200_OK)

    def delete(self, request, personal_id):
        log_request_information(request, "basic")
        self.personal_dao.delete_entry(Personal, personal_id)

        logger.info("Successfully deleted personal %s" % personal_id)
        return string_response("Successfully deleted personal %s" % personal_id, status_code=status.HTTP_200_OK)


class PersonalOrderSet(viewsets.ViewSet):

    authentication_classes = (TokenAuth,)
    permission_classes = (IsAuthenticated,)

    def __init__(self, **kwargs):
        super(PersonalOrderSet, self).__init__(**kwargs)
        self.personal_order_dao = PersonalOrderDAO()

    def get(self, request):
        log_request_information(request, "basic")
        params = self.personal_order_dao.get_search_params(request)

        return_list = []

        if not params:
            personal = self.personal_order_dao.get_all_by_request(PersonalOrder)
        else:
            personal = self.personal_order_dao.get_by_search_fields(PersonalOrder, params)

        for order in personal:
            if not order['returned']:
                doc_id = order['order_document']
                personal = get_object_or_404(Personal, pk=doc_id)
                order['ordered_name'] = "%s, %s" % (personal.last_name, personal.first_name)
                return_list.append(order)

        logger.info("Successfully fetched %s personal order docs" % len(return_list))
        return content_response(return_list, status_code=status.HTTP_200_OK)

    @staticmethod
    def retrieve(request, personal_order_id):
        log_request_information(request, "basic")
        personal_order = get_object_or_404(PersonalOrder, pk=personal_order_id)

        logger.info("Successfully retrieved personal order doc %s" % personal_order_id)
        return content_response(personal_order, status_code=status.HTTP_200_OK)

    def return_order(self, request, personal_order_id):
        log_request_information(request, "basic")

        personal_order = self.personal_order_dao.return_order(personal_order_id)

        logger.info("Successfully set personal order doc %s to returned" % personal_order_id)
        return content_response(model_to_dict(personal_order), status_code=status.HTTP_200_OK)

    def pending_order(self, request, personal_order_id):
        log_request_information(request, "basic")

        personal_order = self.personal_order_dao.pending_order(personal_order_id)

        logger.info("Successfully set personal order doc %s to pending" % personal_order_id)
        return content_response(model_to_dict(personal_order), status_code=status.HTTP_200_OK)

    def create_order(self, request):
        log_request_information(request, "basic")

        personal_order = self.personal_order_dao.create_personal_order(request)

        logger.info("Successfully created personal order doc %s" % personal_order.id)
        return content_response(model_to_dict(personal_order), status_code=status.HTTP_201_CREATED)
