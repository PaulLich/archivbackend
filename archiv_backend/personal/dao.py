import operator
import logging
from datetime import datetime

from django.db.models import Q
from django.shortcuts import get_object_or_404

import archiv_backend.keys as k
from archiv_backend import exceptions, error_codes
from archiv_backend.base_dao import BaseDAO
from models import Personal, PersonalOrder, PersonalFTS

logger = logging.getLogger(__name__)


class PersonalDAO(BaseDAO):

    def __init__(self):
        pass

    # Create or update Personal doc
    @staticmethod
    def create_or_update_personal(payload, this_id=None):

        if this_id:
            logger.info("Update personal doc %s with payload %s" % (this_id, payload))
            personal = Personal.objects.get(pk=int(this_id))
        else:
            logger.info("Create new personal doc with payload %s" % payload)
            personal = Personal()

        personal.archive_number = payload.get(k.ARCHIVE_NUMBER)
        personal.first_name = payload.get(k.FIRST_NAME)
        personal.last_name = payload.get(k.LAST_NAME)
        personal.title = payload.get(k.TITLE)
        personal.name_of_birth = payload.get(k.NAME_OF_BIRTH)
        personal.place_of_birth = payload.get(k.PLACE_OF_BIRTH)
        personal.start_of_employment = payload.get(k.START_OF_EMPLOYMENT)
        personal.end_of_employment = payload.get(k.END_OF_EMPLOYMENT)
        personal.job_title = payload.get(k.JOB_TITLE)

        if payload.get(k.DATE_OF_BIRTH):
            personal.date_of_birth = payload.get(k.DATE_OF_BIRTH)

        if payload.get(k.DATE_OF_DEATH):
            personal.date_of_death = payload.get(k.DATE_OF_DEATH)
            logger.debug(personal.date_of_death)

        personal.save()

        if this_id:
            personal_fts = PersonalFTS.objects.filter(personal=personal).first()
            if personal_fts is None:
                personal_fts = PersonalFTS()

        else:
            personal_fts = PersonalFTS()

        personal_fts.personal = personal
        personal_fts.content = u'\n'.join((unicode(x) for x in filter(None, (personal.first_name,
                                                                             personal.last_name,
                                                                             personal.name_of_birth,
                                                                             personal.date_of_birth,
                                                                             personal.place_of_birth,
                                                                             personal.start_of_employment,
                                                                             personal.end_of_employment,
                                                                             personal.job_title,
                                                                             personal.archive_number,
                                                                             personal.date_of_death,
                                                                             personal.title,
                                                                             ))))

        personal_fts.save()
        return personal

    # Perform a free text search on personal
    def perform_fts(self, request):
        query_string = request.query_params.get('search_term')
        if query_string == u'*':
            return self.prepare_list_response(list(Personal.objects.all()))
        if query_string is not None and query_string is not u'':
            query_list = query_string.split()

            received_objects = PersonalFTS.objects.filter(
                reduce(operator.and_, (Q(content__icontains=x) for x in query_list))).select_related('personal')

            personal_list = []
            for e in received_objects:
                personal_list.append(e.personal)

            prepared_list = self.prepare_list_response(personal_list)

            return prepared_list
        else:
            return []


class PersonalOrderDAO(BaseDAO):

    def __init__(self):
        pass

    # Creates a new personal order document
    @staticmethod
    def create_personal_order(request):

        personal_order = PersonalOrder()
        payload = request.data

        doc_id = payload.get(k.ORDER_DOCUMENT)

        pending_orders = PersonalOrder.objects.filter(returned__exact=False)
        pending_orders = pending_orders.filter(order_document_id__exact=doc_id)

        for order in pending_orders:
            if not order.returned:
                logger.error("Document %s is not returned yet" % doc_id)
                raise exceptions.ValidationError("Document is not returned yet",
                                                 error_code=error_codes.DOCUMENT_IS_NOT_RETURNED)

        personal_order.order_document = get_object_or_404(Personal, pk=payload.get(k.ORDER_DOCUMENT))
        personal_order.user_id = request.user.id
        personal_order.user_first_name = request.user.first_name
        personal_order.user_last_name = request.user.last_name
        personal_order.user_username = request.user.username
        personal_order.order_date = datetime.now()
        personal_order.returned = False
        personal_order.pending = True

        personal_order.save()

        return personal_order

    @staticmethod
    def return_order(personal_order_id):
        personal_order = get_object_or_404(PersonalOrder, pk=personal_order_id)

        personal_order.returned = True
        personal_order.return_date = datetime.now()

        personal_order.save()
        return personal_order

    @staticmethod
    def pending_order(personal_order_id):
        personal_order = get_object_or_404(PersonalOrder, pk=personal_order_id)

        personal_order.pending = False

        personal_order.save()
        return personal_order
