__author__ = 'paullichtenberger'

# General
MISSING_NON_OPTIONAL_ATTRIBUTE              = 1001

# Users

NO_ROLES_PROVIDED                           = 2001
INVALID_ROLE_PROVIDED                       = 2002
USER_NOT_REGISTERED                         = 2003

# Database

OBJECT_WITH_KEY_ALREADY_EXISTS              = 3001
DUPLICATE_EMAIL_ADRESS                      = 3002
DOCUMENT_IS_NOT_RETURNED                    = 3003
