from __future__ import unicode_literals

ID = "id"
TYPE = "type"

DOC = 'doc'
VIEW = 'view'

# Users

ROLES = 'roles'
USER_PASSWORD = 'user_password'
SALT = 'salt'
ADMIN = 'admin'

STUDENT = 'student'
PERSONAL = 'personal'

PRE_EXAM = 'EX-'
PRE_STUD = 'ST-'
PRE_PERS = 'PE-'
EXAM = 'exam'

ARCHIVE_NUMBER = 'archive_number'
SIGNATURE = 'signature'

LAST_NAME = 'last_name'
FIRST_NAME = 'first_name'
TITLE = 'title'
DATE_OF_BIRTH = 'date_of_birth'
PLACE_OF_BIRTH = 'place_of_birth'
NAME_OF_BIRTH = 'name_of_birth'
DATE_OF_DEATH = 'date_of_death'
START_OF_EMPLOYMENT = 'start_of_employment'
END_OF_EMPLOYMENT = 'end_of_employment'
JOB_TITLE = 'job_title'

STUDENT_NUMBER = 'student_number'
FINAL_DEGREE = 'finalDegree'
DEPARTMENT = 'department'
MAIN_SUBJECT = 'main_subject'
SECONDARY_SUBJECT = 'secondary_subject'
DEGREE_SUBJECT = 'degree_subject'
ACADEMIC_TITLE = 'academic_title'
BEGIN_OF_STUDIES = 'begin_of_studies'
END_OF_STUDIES = 'end_of_studies'

BOX_NUMBER = 'box_number'
BOX_NUMBER_OLD = 'box_number_old'
IMMATRICULATION = 'immatriculation'
EXMATRICULATION = 'exmatriculation'
FILE_NUMBER = 'file_number'
DATE_OF_FILING = 'date_of_filing'
ANNOTATIONS = 'annotations'
ALSO_CONTAINS = 'also_contains'

ORDER_DOCUMENT = 'order_document'
ORDER_DATE = 'order_date'
RETURN_DATE = 'return_date'
RETURNED = 'returned'

USER_NAME = 'user_name'
USER_EMAIL = 'user_email'
USER_ROLE = 'user_role'
USER_FIRST_NAME = 'user_first_name'
USER_LAST_NAME = 'user_last_name'

SEARCH_TERM = 'search_term'
