from django.db import models

import logging
import archiv_backend.error_codes as ec
import archiv_backend.keys as k
from archiv_backend.exceptions import ValidationError

logger = logging.getLogger(__name__)

USER_ROLES = ['admin', 'employee', 'personal', 'student', 'exam']


class ArchivUser(models.Model):

    read_repair_chance = 0.05
    user_name = models.CharField(max_length=255)
    user_role = models.CharField(max_length=255)
    user_first_name = models.CharField(max_length=255,
                                       default="")
    user_last_name = models.CharField(max_length=255,
                                      default="")
    login_token = models.CharField(max_length=255,
                                   blank=True,
                                   null=True)

    @staticmethod
    def validate_new_user(user):
        user_name = user.get(k.USER_NAME)
        role = user.get(k.USER_ROLE)

        if not role:
            logger.error("No user role provided when creating new user")
            raise ValidationError("No user role provided", error_code=ec.NO_ROLES_PROVIDED)

        if role not in USER_ROLES:
            logger.error("Invalid role provided when creating new user")
            raise ValidationError("Invalid user role provided", error_code=ec.INVALID_ROLE_PROVIDED)

        user = list(ArchivUser.objects.filter(user_name__iexact=user_name))
        if len(user) > 0:
            logger.error("Duplicate use of username %s" % user_name)
            raise ValidationError("Duplicate use of username", error_code=ec.DUPLICATE_EMAIL_ADRESS)
