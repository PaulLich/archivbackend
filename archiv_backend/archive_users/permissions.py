from rest_framework import permissions


class ArchivUserPermissions(permissions.BasePermission):
    def has_permission(self, request, view):

        # Deny permission to everyone but admins
        user = request.user.details
        if user["user_role"] == "admin":
            return True
        else:
            return False
