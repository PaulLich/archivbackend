import logging

from django.forms import model_to_dict
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated

from archiv_backend.archive_users.models import ArchivUser
from archiv_backend.authentication import BasicAuthentication, TokenAuth, log_request_information
from archiv_backend.response import content_response, string_response
from dao import UserDAO

logger = logging.getLogger(__name__)


class UserSet(viewsets.ViewSet):

    authentication_classes = (BasicAuthentication, TokenAuth)
    permission_classes = (IsAuthenticated,)

    def __init__(self, **kwargs):
        super(UserSet, self).__init__(**kwargs)
        self.user_dao = UserDAO()

    def get(self, request):
        log_request_information(request, "basic")

        params = self.user_dao.get_search_params(request)

        if not params:
            personal = self.user_dao.get_all_by_request(ArchivUser)
        else:
            personal = self.user_dao.get_by_search_fields(ArchivUser, params)

        return content_response(personal, status_code=status.HTTP_200_OK)

    def post(self, request):
        log_request_information(request, "basic")

        payload = request.data

        ArchivUser.validate_new_user(payload)

        user = self.user_dao.create_user(payload)

        return content_response(model_to_dict(user), status_code=status.HTTP_201_CREATED)

    def delete(self, request, user_id):
        log_request_information(request, "basic")
        self.user_dao.delete_user(user_id)

        logger.info("Successfully Deleted User %s" % user_id)
        return string_response("Successfully deleted user %s" % user_id, status_code=status.HTTP_200_OK)

    def login(self, request):
        log_request_information(request, "basic")
        user = self.user_dao.login_and_create_token(request.user)
        user = model_to_dict(user)
        user["email"] = request.user.email
        logger.info("User %s successfully logged in" % user)
        response = content_response(user, status_code=status.HTTP_200_OK)
        return response

    def logout(self, request):
        log_request_information(request, "basic")
        user = self.user_dao.logout(request.user.token)
        logger.info("User %s successfully logged out" % user)
        response = content_response(model_to_dict(user), status_code=status.HTTP_200_OK)
        return response
