import logging
import uuid

from archiv_backend import exceptions, error_codes

from archiv_backend import keys as k
from archiv_backend.base_dao import BaseDAO
from models import ArchivUser

logger = logging.getLogger(__name__)


class UserDAO(BaseDAO):
    def __init__(self):
        pass

    # Create new User
    @staticmethod
    def create_user(payload):
        logger.info("Create User %s" % payload)

        new_user = ArchivUser(user_name=payload.get(k.USER_NAME),
                              user_role=payload.get(k.USER_ROLE),
                              user_first_name=payload.get(k.USER_FIRST_NAME),
                              user_last_name=payload.get(k.USER_LAST_NAME))
        new_user.save()

        return new_user

    # Delete User
    @staticmethod
    def delete_user(user_id):
        logger.info("Delete User %s" % user_id)

        # Try do delete User
        user = ArchivUser.objects.get(pk=user_id)
        user.delete()

    @staticmethod
    def login_and_create_token(user):

        # Find user in db and create login token
        try:
            user = list(ArchivUser.objects.filter(user_name__exact=user.username))[0]
        except IndexError:
            raise exceptions.PermissionDenied("Username not available", error_code=error_codes.USER_NOT_REGISTERED)

        uid = uuid.uuid4()
        user.login_token = uid

        user.save()

        return user

    @staticmethod
    def logout(user_token):

        # Remove user login token from db
        user = list(ArchivUser.objects.filter(login_token__exact=user_token))[0]
        user.login_token = None
        user.save()

        return user
