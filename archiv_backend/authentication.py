import base64
import os

from django.contrib.auth.models import User
from django.forms import model_to_dict

from archiv_backend.archive_users.models import ArchivUser
from rest_framework import authentication, exceptions, HTTP_HEADER_ENCODING
import logging
import ldap
from rest_framework.authentication import get_authorization_header

from backend import settings

logger = logging.getLogger(__name__)

def log_request_information(request, auth_type):
    dev_ip = request._request.META.get('REMOTE_ADDR', None)
    prod_ip = request._request.META.get('HTTP_X_FORWARDED_FOR', None)

    path = request._request.path + format_query_params_for_logging(request.query_params)

    logging_args = {
        'method': request.method,
        'path': path,
        'ip': prod_ip or dev_ip,
        'auth_type': auth_type
    }
    logger.debug('%(ip)s | %(auth_type)s | %(method)s %(path)s' % logging_args)


def format_query_params_for_logging(query_params):
    params = ['='.join(pair) for pair in query_params.items()]

    return '?' + '&'.join(params) if params else ''

class TokenAuth(authentication.TokenAuthentication):
    def authenticate(self, request):
        log_request_information(request, 'token')
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != b'token':
            msg = 'Invalid header. Only Token Auth allowed.'
            return None

        if len(auth) == 1:
            msg = 'Invalid basic header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid basic header. Credentials string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)

        token = auth[1]

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, token):
        user = list(ArchivUser.objects.filter(login_token__exact=token))

        if not user:
            return None

        username = user[0].user_name
        user_id = user[0].id
        first_name = user[0].user_first_name
        last_name = user[0].user_last_name

        d_user = User()

        d_user.username = username
        d_user.first_name = first_name
        d_user.last_name = last_name
        d_user.id = user_id
        d_user.token = token

        return (d_user, None)

class BasicAuthentication(authentication.BasicAuthentication):
    def authenticate(self, request):
        log_request_information(request, 'basic')
        auth = get_authorization_header(request).split()
        logger.debug(get_authorization_header(request))

        if not auth or auth[0].lower() != b'basic':
            logger.debug(auth)
            return None

        if len(auth) == 1:
            msg = _('Invalid basic header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid basic header. Credentials string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            auth_parts = base64.b64decode(auth[1]).decode(HTTP_HEADER_ENCODING).partition(':')
        except (TypeError, UnicodeDecodeError):
            msg = _('Invalid basic header. Credentials not correctly base64 encoded.')
            raise exceptions.AuthenticationFailed(msg)

        userid, password = auth_parts[0], auth_parts[2]

        return self.authenticate_credentials(userid, password)

    def authenticate_credentials(self, userid, password):

        #ldap.set_option(ldap.OPT_REFERRALS, 0)
        #ldap.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
        ldap.set_option(ldap.OPT_DEBUG_LEVEL, 255)
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        if settings.SERVER_ENVIRONMENT == "production":
            ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, str(settings.runtime_settings["cert"]))
            logger.debug(str(settings.runtime_settings["cert"]))
            logger.warn(str(settings.runtime_settings["cert"]))
            logger.info(str(settings.runtime_settings["cert"]))
            logger.error(str(settings.runtime_settings["cert"]))
        #ldap.set_option(ldap.OPT_X_TLS,ldap.OPT_X_TLS_DEMAND)
        #ldap.set_option(ldap.OPT_X_TLS_DEMAND, True)

        logger.debug("Starting Authenticate")

        try:
            ld = ldap.initialize("ldaps://ldap.uni-regensburg.de")
        except ldap.SERVER_DOWN:
            print("LDAP Server Down")

        res = ld.search_s('c=de', ldap.SCOPE_SUBTREE, "(cn=%s)" % userid)

        if not res:
            raise exceptions.AuthenticationFailed("User does not exist")

        dn = res[0][0]

        logger.debug("LDAP initialized")

        try:
            u_obj = ld.bind_s(dn, password)
        except ldap.INVALID_CREDENTIALS:
            raise exceptions.NotAuthenticated("LDAP auth failed")

        #print("Res: %s" % u_obj)

        user = ArchivUser.objects.filter(user_name__exact = userid)

        #logger.debug("%s" % user)
        #user = model_to_dict(user)

        

        d_user = User()

        d_user.username = userid
        d_user.email = res[0][1]["mail"][0]

        return (d_user, None)

