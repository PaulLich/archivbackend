from django.conf.urls import url
from archiv_backend.exam.views import ExamSet, ExamOrderSet
from archiv_backend.personal.views import PersonalSet, PersonalOrderSet
from archiv_backend.student.views import StudentSet, StudentOrderSet
from archiv_backend.archive_users.views import UserSet

get_post = {
    'get': 'get',
    'post': 'post'
}

retrieve_put = {
    'get': 'retrieve',
    'put': 'put',
    'delete': 'delete'
}

search = {
    'get': 'search'
}

return_order = {
    'put': 'return_order'
}

pending_order = {
    'put': 'pending_order'
}

get_create_order = {
    'get': 'get',
    'post': 'create_order'
}

retrieve = {
    'get': 'retrieve'
}

delete = {
    'delete': 'delete'
}

urlpatterns = [
    # Exams and Exam Orders

    url(r'^exam_orders/return_order/([a-zA-Z0-9-_\?]{1,50})/?$', ExamOrderSet.as_view(return_order)),
    url(r'^exam_orders/pending_order/([a-zA-Z0-9-_\?]{1,50})/?$', ExamOrderSet.as_view(pending_order)),
    url(r'^exam_orders/([a-zA-Z0-9-_\?]{1,50})/?$', ExamOrderSet.as_view(retrieve)),
    url(r'^exam_orders/?$', ExamOrderSet.as_view(get_create_order)),

    url(r'^exams/([a-zA-Z0-9-_\?]{1,50})/?$', ExamSet.as_view(retrieve_put)),
    url(r'^exams/?$', ExamSet.as_view(get_post)),
    url(r'^searchexams/?$', ExamSet.as_view(search)),


    # Students and Student Orders

    url(r'^student_orders/return_order/([a-zA-Z0-9-_\?]{1,50})/?$', StudentOrderSet.as_view(return_order)),
    url(r'^student_orders/pending_order/([a-zA-Z0-9-_\?]{1,50})/?$', StudentOrderSet.as_view(pending_order)),
    url(r'^student_orders/([a-zA-Z0-9-_\?]{1,50})/?$', StudentOrderSet.as_view(retrieve)),
    url(r'^student_orders/?$', StudentOrderSet.as_view(get_create_order)),

    url(r'^students/([a-zA-Z0-9-_\?]{1,50})/?$', StudentSet.as_view(retrieve_put)),
    url(r'^students/?$', StudentSet.as_view(get_post)),

    url(r'^searchstudents/?$', StudentSet.as_view(search)),

    # Personal and Personal Orders

    url(r'^personal_orders/return_order/([a-zA-Z0-9-_\?]{1,50})/?$', PersonalOrderSet.as_view(return_order)),
    url(r'^personal_orders/pending_order/([a-zA-Z0-9-_\?]{1,50})/?$', PersonalOrderSet.as_view(pending_order)),
    url(r'^personal_orders/([a-zA-Z0-9-_\?]{1,50})/?$', PersonalOrderSet.as_view(retrieve)),
    url(r'^personal_orders/?$', PersonalOrderSet.as_view(get_create_order)),

    url(r'^personal/([a-zA-Z0-9-_\?]{1,50})/?$', PersonalSet.as_view(retrieve_put)),
    url(r'^personal/?$', PersonalSet.as_view(get_post)),

    url(r'^searchpersonal/?$', PersonalSet.as_view(search)),

    # User Management Endpoints

    url(r'^users/?$', UserSet.as_view(get_post)),
    url(r'^users/([a-zA-Z0-9-_\?]{1,50})/?$', UserSet.as_view(delete)),

    url(r'^login/?$', UserSet.as_view({'get': 'login'})),
    url(r'^logout/?$', UserSet.as_view({'get': 'logout'})),
    ]
